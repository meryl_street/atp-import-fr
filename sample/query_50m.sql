
    WITH matched_points AS (
        SELECT 
            p.osm_id,
            p.name AS OSM_name, 
            'https://www.osm.org/node/' || p.osm_id AS OSM_url,
            COALESCE(p."shop", p."amenity", p."office", p."healthcare") AS OSM_business,
            n."addr:city" AS ATP_city,
            n."addr:street_address" AS ATP_address, 
            COALESCE(p."addr:housenumber", p."contact:housenumber") AS OSM_housenumber,
            COALESCE(p."addr:street", p."contact:street") AS OSM_street,
            n.phone AS ATP_phone, 
            COALESCE(p."phone", p."contact:phone") AS OSM_phone,
            n.opening_hours AS ATP_opening_hours, 
            p."opening_hours" AS OSM_hours,
            n.website AS ATP_website, 
            COALESCE(p."website", p."contact:website") AS OSM_website,
            n."brand:wikidata" AS brand_wikidata,
            n.brand AS brand,
            COUNT(*) OVER (PARTITION BY p.osm_id) AS match_count
        FROM 
            atptable n
        JOIN 
            planet_osm_point p
        ON 
            ST_DWithin(
                ST_Transform(n.wkb_geometry, 3857),  -- Transform geometry to EPSG:3857
                p.way, 
                50
            )
            AND n."brand:wikidata" = p."brand:wikidata"  -- Ensure brands match
        WHERE
            n."addr:country" = 'FR'
            AND COALESCE(p."shop", p."amenity", p."office", p."healthcare") IS NOT NULL
            AND LOWER(n."addr:city") != 'monaco'
            AND n.website IS NOT NULL
            AND (LENGTH(n.phone) <= 17 OR n.phone IS NULL)
            AND p.osm_id >= 0
            AND POSITION(';' IN COALESCE(p."shop", p."amenity", p."office", p."healthcare")) = 0
            AND COALESCE(p."shop", p."amenity", p."office", p."healthcare") != 'atm'
            AND COALESCE(p."shop", p."amenity", p."office", p."healthcare") != 'fuel'
            AND COALESCE(p."shop", p."amenity", p."office", p."healthcare") != 'gas'
           
    ),
    matched_polygons AS (
        SELECT 
            p.osm_id,
            p.name AS OSM_name, 
            'https://www.osm.org/way/' || p.osm_id AS OSM_url,
            COALESCE(p."shop", p."amenity", p."office", p."healthcare") AS OSM_business,
            n."addr:city" AS ATP_city,
            n."addr:street_address" AS ATP_address, 
            COALESCE(p."addr:housenumber", p."contact:housenumber") AS OSM_housenumber,
            COALESCE(p."addr:street", p."contact:street") AS OSM_street,
            n.phone AS ATP_phone, 
            COALESCE(p."phone", p."contact:phone") AS OSM_phone,
            n.opening_hours AS ATP_opening_hours, 
            p."opening_hours" AS OSM_hours,
            n.website AS ATP_website, 
            COALESCE(p."website", p."contact:website") AS OSM_website,
            n."brand:wikidata" AS brand_wikidata,
            n.brand AS brand,
            COUNT(*) OVER (PARTITION BY p.osm_id) AS match_count
        FROM 
            atptable n
        JOIN 
            planet_osm_polygon p
        ON 
            ST_DWithin(
                ST_Transform(n.wkb_geometry, 3857),  -- Transform geometry to EPSG:3857
                p.way, 
                50
            )
            AND n."brand:wikidata" = p."brand:wikidata"  -- Ensure brands match
        WHERE
            n."addr:country" = 'FR'
            AND COALESCE(p."shop", p."amenity", p."office", p."healthcare") IS NOT NULL
            AND LOWER(n."addr:city") != 'monaco'
            AND n.website IS NOT NULL
            AND (LENGTH(n.phone) <= 17 OR n.phone IS NULL)
            AND p.osm_id >= 0
            AND POSITION(';' IN COALESCE(p."shop", p."amenity", p."office", p."healthcare")) = 0
            AND COALESCE(p."shop", p."amenity", p."office", p."healthcare") != 'atm'
            AND COALESCE(p."shop", p."amenity", p."office", p."healthcare") != 'fuel'
            AND COALESCE(p."shop", p."amenity", p."office", p."healthcare") != 'gas'
           
    )
    
        ,
        combined_data AS (
            SELECT * FROM matched_points WHERE match_count = 1
            UNION ALL
            SELECT * FROM matched_polygons WHERE match_count = 1
        )
        , duplicate_urls AS (
            SELECT OSM_url
            FROM combined_data
            GROUP BY OSM_url
            HAVING COUNT(*) > 1
        )
        SELECT *
        FROM combined_data
        WHERE OSM_url NOT IN (SELECT OSM_url FROM duplicate_urls)
        