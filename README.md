# atp-import-fr

Import des données [All The Places](https://www.alltheplaces.xyz/) dans OSM.

Ce script se focalise sur les chaines de magasins francaises et permet de contribuer les attributs website/telephone/opening_hours manquants sur des objets OSM existants.

Le script fonctionne avec des informations provenant d'une base de données Postgresql/PostGIS et ne fonctionne pas seul.

Plus d'information sur la page [wiki](https://wiki.openstreetmap.org/wiki/Import/All_the_Places_FR_data)