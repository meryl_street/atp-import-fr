import argparse
import configparser
import json
import overpass
import xml.etree.ElementTree as XMLet
import sys
import os
import psycopg2
import shutil
from datetime import datetime


def download_data_from_overpass(wikidata_num, biz_type):

    # Define the Overpass API query
    query = f"""
    area["admin_level"="3"]["name"="France métropolitaine"]->.france;

    // Find nodes and ways with the specified tag within the area
    // Filter further by shop/amenity type to prevent wrong contribution
    (
      node[{biz_type}]["brand:wikidata"="{wikidata_num}"](area.france);
      way[{biz_type}]["brand:wikidata"="{wikidata_num}"](area.france);
    );

    out meta;
    >;
    out meta qt;
    """

    # Execute the query
    api = overpass.API(timeout=120)
    api_response = api.get(query, responseformat='xml', build=False)

    # Save the response to a file
    with open(overpass_file, "w", encoding='utf-8') as file:
        file.write(api_response)

    print_and_log(f"  Written to {overpass_file}", log_list)


def get_shop_amenity_type(wikidata_ref):
    # Getting that info from ATP (it's one of the fields)

    query_biz_type = f"""
    WITH first_10000_rows AS (
        SELECT "shop", "amenity"
        FROM "atptable"
        WHERE "brand:wikidata" = '{wikidata_ref}'
        LIMIT 10000
    )
    SELECT 
        'shop' AS biz_type, 
        shop AS biz_value, 
        COUNT(*) AS count
    FROM first_10000_rows
    WHERE shop IS NOT NULL
    GROUP BY shop

    UNION ALL

    SELECT 
        'amenity' AS biz_type, 
        amenity AS biz_value, 
        COUNT(*) AS count
    FROM first_10000_rows
    WHERE amenity IS NOT NULL
    GROUP BY amenity
    ORDER BY count DESC;
    """

    colnames, results = execute_query(query_biz_type)

    if results:
        # Extract the first row
        first_row = results[0]

        # Find the indices of 'biz_type' and 'biz_value'
        biz_type_index = colnames.index('biz_type')
        biz_value_index = colnames.index('biz_value')

        # Extract the values
        biz_type = first_row[biz_type_index]
        biz_value = first_row[biz_value_index]
        return f"'{biz_type}'='{biz_value}'"
    else:
        print("No data found")
        return None, None


def construct_query(atp_table_name,
                    osm_shop_amenity_type,
                    remove_duplicates,
                    filter_osm_relations,
                    filter_monaco,
                    ignore_poi_types,
                    remove_doubled_poi_type,
                    filter_website,
                    filter_phone_length,
                    distance
                    ):

    osm_shop_amenity_type = osm_shop_amenity_type.replace("'", "")
    osm_type, osm_type_value = osm_shop_amenity_type.split('=')

    where_clauses = []

    if filter_monaco:
        where_clauses.append('LOWER(n."addr:city") != \'monaco\'\n           ')
    if filter_website:
        where_clauses.append('n.website IS NOT NULL\n           ')
    if filter_phone_length:
        where_clauses.append('(LENGTH(n.phone) <= 17 OR n.phone IS NULL)\n           ')
    if filter_osm_relations:
        where_clauses.append('p.osm_id >= 0\n           ')
    if remove_doubled_poi_type:
        where_clauses.append('POSITION(\';\' IN COALESCE(p."shop", p."amenity", '
                             'p."office", p."healthcare")) = 0\n           ')
    if ignore_poi_types:
        for poi_type in ignore_poi_types:
            where_clauses.append(f'COALESCE(p."shop", p."amenity", p."office", '
                                 f'p."healthcare") != \'{poi_type}\'\n           ')

    where_statement = ' AND '.join(where_clauses)

    query = f"""
    WITH matched_points AS (
        SELECT 
            p.osm_id,
            p.name AS OSM_name, 
            'https://www.osm.org/node/' || p.osm_id AS OSM_url,
            COALESCE(p."shop", p."amenity", p."office", p."healthcare") AS OSM_business,
            n."addr:city" AS ATP_city,
            n."addr:street_address" AS ATP_address, 
            COALESCE(p."addr:housenumber", p."contact:housenumber") AS OSM_housenumber,
            COALESCE(p."addr:street", p."contact:street") AS OSM_street,
            n.phone AS ATP_phone, 
            COALESCE(p."phone", p."contact:phone") AS OSM_phone,
            n.opening_hours AS ATP_opening_hours, 
            p."opening_hours" AS OSM_hours,
            n.website AS ATP_website, 
            COALESCE(p."website", p."contact:website") AS OSM_website,
            n."brand:wikidata" AS brand_wikidata,
            n.brand AS brand,
            n.shop as osm_shop,
            n.amenity as osm_amenity,
            COUNT(*) OVER (PARTITION BY p.osm_id) AS match_count
        FROM 
            {atp_table_name} n
        JOIN 
            planet_osm_point p
        ON 
            ST_DWithin(
                ST_Transform(n.wkb_geometry, 3857),  -- Transform geometry to EPSG:3857
                p.way, 
                {distance}
            )
            AND n."brand:wikidata" = p."brand:wikidata"  -- Ensure brands match
        WHERE
            n."addr:country" = 'FR'
            AND n."{osm_type}" = '{osm_type_value}'
            AND COALESCE(p."shop", p."amenity", p."office", p."healthcare") IS NOT NULL
            {f'AND {where_statement}' if where_statement else ''}
    ),
    matched_polygons AS (
        SELECT 
            p.osm_id,
            p.name AS OSM_name, 
            'https://www.osm.org/way/' || p.osm_id AS OSM_url,
            COALESCE(p."shop", p."amenity", p."office", p."healthcare") AS OSM_business,
            n."addr:city" AS ATP_city,
            n."addr:street_address" AS ATP_address, 
            COALESCE(p."addr:housenumber", p."contact:housenumber") AS OSM_housenumber,
            COALESCE(p."addr:street", p."contact:street") AS OSM_street,
            n.phone AS ATP_phone, 
            COALESCE(p."phone", p."contact:phone") AS OSM_phone,
            n.opening_hours AS ATP_opening_hours, 
            p."opening_hours" AS OSM_hours,
            n.website AS ATP_website, 
            COALESCE(p."website", p."contact:website") AS OSM_website,
            n."brand:wikidata" AS brand_wikidata,
            n.brand AS brand,
            n.shop as osm_shop,
            n.amenity as osm_amenity,
            COUNT(*) OVER (PARTITION BY p.osm_id) AS match_count
        FROM 
            {atp_table_name} n
        JOIN 
            planet_osm_polygon p
        ON 
            ST_DWithin(
                ST_Transform(n.wkb_geometry, 3857),  -- Transform geometry to EPSG:3857
                p.way, 
                {distance}
            )
            AND n."brand:wikidata" = p."brand:wikidata"  -- Ensure brands match
        WHERE
            n."addr:country" = 'FR'
            AND n."{osm_type}" = '{osm_type_value}'
            AND COALESCE(p."shop", p."amenity", p."office", p."healthcare") IS NOT NULL
            {f'AND {where_statement}' if where_statement else ''}
    )
    """

    if not remove_duplicates:
        query += """
        SELECT * FROM matched_points
        UNION ALL
        SELECT * FROM matched_polygons
        """
    else:
        query += """
        ,
        combined_data AS (
            SELECT * FROM matched_points WHERE match_count = 1
            UNION ALL
            SELECT * FROM matched_polygons WHERE match_count = 1
        )
        , duplicate_urls AS (
            SELECT OSM_url
            FROM combined_data
            GROUP BY OSM_url
            HAVING COUNT(*) > 1
        )
        SELECT *
        FROM combined_data
        WHERE OSM_url NOT IN (SELECT OSM_url FROM duplicate_urls)
        """

    return query


def get_pois_with_brandwikidata(wikidata_ref, brand_type, dict_param):

    dbquery = construct_query(overall_table,
                            brand_type,
                            remove_duplicates=dict_param['bl_remove_duplicates'],
                            filter_osm_relations=dict_param['bl_filter_osm_relations'],
                            filter_monaco=dict_param['bl_filter_monaco'],
                            ignore_poi_types=dict_param['pois_to_remove'],
                            remove_doubled_poi_type=dict_param['bl_remove_doubled_poi_type'],
                            filter_website=dict_param['bl_filter_website'],
                            filter_phone_length=dict_param['bl_filter_phone_length'],
                            distance=dict_param['poi_distance_atp_osm']
                              )

    # print(dbquery)  # Print the dbquery for debugging
    # Write the db query to a file
    try:
        with open(sql_query_file, 'w') as file:
            file.write(dbquery)
            print_and_log(f"  Saved query to {sql_query_file}", log_list)
    except Exception as e:
        print(f"  Error occurred while writing to file: {e}", file=sys.stderr)
        sys.exit(1)

    colnames, results = execute_query(dbquery)
    
    print_and_log("  Queried database", log_list)
    name_of_brand = ''

    if colnames and results:
        json_results = [dict(zip(colnames, row)) for row in results]
        filtered_results = [item for item in json_results if item.get('brand_wikidata') == wikidata_ref]

        if filtered_results:
            name_of_brand = f"{filtered_results[0]['brand']}"
            with open(output_json_file, 'w') as f:
                json.dump(filtered_results, f, indent=4)
            print_and_log(f"  Results written to {output_json_file}", log_list)
        else:
            print_and_log(f"  No results found for brand_wikidata: {wikidata_ref}", log_list)
    else:
        print_and_log("  No results returned or an error occurred.", log_list)

    return name_of_brand, output_json_file


def execute_query(query):
    try:

        # Establish connection to the database
        conn = psycopg2.connect(
            dbname=vardbname,
            user=varuser,
            password=varpassword,
            host=varhost
        )
        cur = conn.cursor()

        cur.execute(query)
        results = cur.fetchall()

        # Fetch column headers
        colnames = [desc[0] for desc in cur.description]

        cur.close()
        conn.close()
        
        return colnames, results
    except Exception as e:
        print_and_log(f"  Error: {e}", log_list)
        return None, None


def add_missing_data(original_file, final_file, json_file):
    tree = XMLet.parse(original_file)
    root = tree.getroot()

    # Gathering stats
    # Count nodes and ways with the specific brand:wikidata tag
    total_items = 0
    for element in root.findall('.//node') + root.findall('.//way'):
        if element.find(f"./tag[@k='brand:wikidata'][@v='{brand_wikidata_ref}']") is not None:
            total_items += 1
    added_website = 0
    added_phone = 0
    added_hours = 0

    # Load the JSON data
    with open(json_file, 'r', encoding='utf-8') as f:
        json_data = json.load(f)

    # Update nodes and ways based on the JSON data
    for item in json_data:
        osm_id = str(item['osm_id'])
        osm_url = item['osm_url']
        atp_phone = item.get('atp_phone')
        atp_website = item.get('atp_website')
        atp_opening_hours = item.get('atp_opening_hours')

        # Ensure atp_website starts with https://
        if atp_website:
            if not atp_website.startswith('https://'):
                if atp_website.startswith('http://'):
                    atp_website = 'https://' + atp_website[len('http://'):]
                elif atp_website.startswith('www.'):
                    atp_website = 'https://' + atp_website
                else:
                    pass

        # Determine if the element is a node or a way based on osm_url
        if 'node' in osm_url:
            element = root.find(f".//node[@id='{osm_id}']")
        elif 'way' in osm_url:
            element = root.find(f".//way[@id='{osm_id}']")
        else:
            continue

        if element is not None:
            # Add phone if not present
            if atp_phone and not tag_exists(element, ['phone', 'contact:phone']):
                XMLet.SubElement(element, 'tag', {'k': 'phone', 'v': atp_phone})
                element.set('action', 'modify')
                added_phone += 1

            # Add website if not present
            if atp_website and not tag_exists(element, ['website', 'contact:website', 'url']):
                XMLet.SubElement(element, 'tag', {'k': 'website', 'v': atp_website})
                element.set('action', 'modify')
                added_website += 1

            # Add opening_hours if not present
            if atp_opening_hours and not tag_exists(element, ['opening_hours']):
                XMLet.SubElement(element, 'tag', {'k': 'opening_hours', 'v': atp_opening_hours})
                element.set('action', 'modify')
                added_hours += 1

    print_and_log(f"  File {final_file} done", log_list)
    prettify(root)

    # Write the pretty printed XML to the new file
    tree.write(final_file, encoding='UTF-8', xml_declaration=True)

    # Return the counts
    return {
        'total_items': total_items,
        'added_website': added_website,
        'added_phone': added_phone,
        'added_hours': added_hours
    }


def tag_exists(element, keys):
    for tag in element.findall('tag'):
        if tag.attrib['k'] in keys:
            return True
    return False


def prettify(element, level=0):
    indent = "  "
    i = "\n" + level * indent
    if len(element):
        if not element.text or not element.text.strip():
            element.text = i + indent
        if not element.tail or not element.tail.strip():
            element.tail = i
        elem = None
        for elem in element:
            prettify(elem, level + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not element.tail or not element.tail.strip()):
            element.tail = i


def move_files_to_folder(foldername):

    destination_dir = f'{foldername}'

    # Remove the directory if it exists
    if os.path.exists(destination_dir):
        shutil.rmtree(destination_dir)

    # Create the directory
    os.makedirs(destination_dir, exist_ok=True)

    # Define the list of files to move
    files_to_move = [json_file_with_merges, overpass_file, sql_query_file, modified_file]

    # Move each file to the destination directory
    for source_file in files_to_move:
        # Check if the source file exists before attempting to move it
        if os.path.exists(source_file):
            destinationfile = os.path.join(destination_dir, os.path.basename(source_file))
            try:
                shutil.move(source_file, destinationfile)
            except Exception as e:
                print(f"  Error occurred while moving the file {source_file}: {e}", file=sys.stderr)
                sys.exit(1)
        else:
            print(f"  Source file does not exist: {source_file}", file=sys.stderr)
            sys.exit(1)

    print_and_log(f"  Moved below files to folder {destination_dir}:", log_list)
    for filename in files_to_move:
        print_and_log(f"  - {filename}", log_list)


# Helper function to print to terminal and log to a file
def print_and_log(message, loglist):
    print(message)
    loglist.append(message)


##########################################################################
log_list = []

# Parse command-line arguments
parser = argparse.ArgumentParser(description='ImportAllThePlaces_FR')
parser.add_argument('brand_wikidata_ref', type=str, help='Wikidata Brand to process')
parser.add_argument('--distance', type=int, help='Conflation Distance ATP/OSM')
args = parser.parse_args()

# Ensure both arguments are provided
if not args.brand_wikidata_ref or not args.distance:
    print_and_log("\033[91mError: Wikidata brand and ATP/OSM conflation distances need to "
                  "be specified.\033[0m", log_list)
    sys.exit(1)

brand_wikidata_ref = args.brand_wikidata_ref
poi_distance_atp_osm = args.distance

# Prompt the user if distance is more than 500
if poi_distance_atp_osm > 500:
    print_and_log("\033[93mWarning: Conflation distance > 500 meters.\nThis might create a lot of "
                  "false-positives.\033[0m", log_list)
    response = input("Are you sure you want to continue? (yes/no): ").strip().lower()
    if response not in ['yes', 'y']:
        print_and_log("  Operation aborted.", log_list)
        exit(1)

today_date = datetime.now().strftime('%Y-%m-%d')

# Read database configuration from config file
config = configparser.ConfigParser()
config.read('script_config.ini')

vardbname = config['database']['dbname']
overall_table = config['database']['dbtable']
varuser = config['database']['user']
varpassword = config['database']['password']
varhost = config['database']['host']

# Read script parameters from config file
script_parameters = {
    'pois_to_remove': config['settings']['pois_to_remove'].split(','),  # Read and split the pois_to_remove list
    'bl_remove_duplicates': config['settings']['bl_remove_duplicates'],
    'bl_filter_osm_relations': config['settings']['bl_filter_osm_relations'],
    'bl_filter_monaco': config['settings']['bl_filter_monaco'],
    'bl_remove_doubled_poi_type':  config['settings']['bl_remove_doubled_poi_type'],
    'bl_filter_website': config['settings']['bl_filter_website'],
    'bl_filter_phone_length': config['settings']['bl_filter_phone_length'],
    'poi_distance_atp_osm': poi_distance_atp_osm
    }

print_and_log("\nFinding shop/amenity type.....", log_list)
business_type = get_shop_amenity_type(brand_wikidata_ref)
print_and_log(f"  {business_type}", log_list)

print_and_log("\nDownloading latest OSM data with Overpass.....", log_list)
overpass_file = f'latest-overpass-{brand_wikidata_ref}.osm'

download_data_from_overpass(brand_wikidata_ref, business_type)

print_and_log("\nExtracting POIs from PostgreSQL.....", log_list)
sql_query_file = f'{brand_wikidata_ref}.sql'    
output_json_file = f'{brand_wikidata_ref}.json'

brand_name, json_file_with_merges = get_pois_with_brandwikidata(brand_wikidata_ref, business_type, script_parameters)

print_and_log("\nBuilding .osm file with modifications.....", log_list)
modified_file = f'upload-josm-{brand_wikidata_ref}-{brand_name}.osm'

stats_contributions = add_missing_data(overpass_file, modified_file, json_file_with_merges)

print_and_log(f"\nCleaning up.....", log_list)
move_files_to_folder(f'data/{today_date}-{brand_wikidata_ref}-{brand_name}')

# ANSI escape codes for coloring
color_start = "\033[92m"  # Green color
color_end = "\033[0m"     # Reset color

print_and_log(f"\nFinished processing {color_start}{brand_name}{color_end}", log_list)
print_and_log(f"  {color_start}{modified_file}{color_end} can now be imported into JOSM....", log_list)

print_and_log("\n##############################################", log_list)

print_and_log(f"\nOut of {stats_contributions['total_items']} existing {brand_name} POIs in OSM, "
              f"the script added:", log_list)
print_and_log(f"  - {stats_contributions['added_website']} website tags", log_list)
print_and_log(f"  - {stats_contributions['added_phone']} phone tags", log_list)
print_and_log(f"  - {stats_contributions['added_hours']} opening_hours tags ", log_list)

print_and_log("\n##############################################", log_list)
print_and_log("\nBelow changeset comments to add to JOSM\n", log_list)

print_and_log(f"  created_by atp-import-fr", log_list)
print_and_log(f"  comment Import website/phone/opening_hours manquants pour {brand_name}", log_list)
print_and_log(f"  import yes", log_list)
print_and_log(f"  source alltheplaces.xyz ", log_list)
print_and_log(f"  url https://wiki.openstreetmap.org/wiki/Import/All_the_Places_FR_data", log_list)

print_and_log("\n##############################################", log_list)

print_and_log("\nCheck List:", log_list)
print_and_log("  - urls resolve", log_list)
print_and_log("  - https://", log_list)
print_and_log("  - fr/ urls", log_list)
print_and_log("  - check 3 opening_hours", log_list)
print_and_log("  - JOSM data validator", log_list)
print_and_log("  - selective upload", log_list)

print("")

# Collect mini sentences in a list
mini_sentences = []

for key, value in script_parameters.items():
    if value == 'True':  # Check if the value is the string 'True'
        mini_sentences.append(f"{key[3:]} OK")
    elif key == 'pois_to_remove':
        mini_sentences.append(f"No {', '.join(value)}")
    elif key == 'poi_distance_atp_osm':
        mini_sentences.append(f"{value} meters conflation distance")

# Write log to HTML file
report_file = f'Report-{today_date}-{brand_wikidata_ref}-{brand_name}.html'

with open(report_file, 'w', encoding='utf-8') as report:
    report.write(f"<html><body><h1>Report {today_date} {brand_wikidata_ref} {brand_name}</h1><ul>")
    report.write(f"<p>{' - '.join(mini_sentences[:4])}</p>")
    report.write(f"<p>{' - '.join(mini_sentences[5:])}</p>")
    for log in log_list:
        if color_start in log:
            log = log.replace(color_start, "<span style='color: green;'>").replace(color_end, "</span>")
        if log.startswith("  "):
            report.write(f"<p>{log.strip()}</p>")
        else:
            report.write(f"<h3>{log}</h3>")
    report.write("</ul></body></html>")

destination_file = os.path.join(f'data/{today_date}-{brand_wikidata_ref}-{brand_name}', os.path.basename(report_file))
shutil.move(report_file, destination_file)
